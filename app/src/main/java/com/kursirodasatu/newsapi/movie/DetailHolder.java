package com.kursirodasatu.newsapi.movie;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.kursirodasatu.newsapi.R;

public class DetailHolder extends RecyclerView.ViewHolder {
    TextView tvDetailKey, tvDetailValue;
    public DetailHolder(View itemView) {
        super(itemView);
        tvDetailKey = (TextView) itemView.findViewById(R.id.tvDetailKey);
        tvDetailValue = (TextView) itemView.findViewById(R.id.tvDetailValue);
    }
}
