package com.kursirodasatu.newsapi.movie;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.kursirodasatu.newsapi.R;
import com.kursirodasatu.newsapi.pojo.movie.Movie;
import com.kursirodasatu.newsapi.retrofit.viewmodel.MoviePageApiViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MoviePageFragment extends Fragment implements MoviePageAdapter.ClickItem {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    Unbinder unbinder;

    @BindView(R.id.floatActionButton)
    FloatingActionButton floatActionButton;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_main, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setHasFixedSize(true);
        final MoviePageAdapter adapter = new MoviePageAdapter(getContext(), this);

        final MoviePageApiViewModel model = ViewModelProviders.of(this).get(MoviePageApiViewModel.class);
        model.getItemPagedList().observe(this, new Observer<PagedList<Movie.Results>>() {
            @Override
            public void onChanged(@Nullable PagedList<Movie.Results> results) {
                adapter.submitList(results);
            }
        });

        recyclerView.setAdapter(adapter);


        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(0, 0, 16, 32);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        params.addRule(RelativeLayout.ALIGN_PARENT_END);
        floatActionButton.setLayoutParams(params);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT
                );
                int aku = floatActionButton.getMeasuredWidth();
                if (dy < 0) {
                    floatActionButton.hide();
                    params.setMargins(0, 0, 24, 32 + aku);
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    params.addRule(RelativeLayout.ALIGN_PARENT_END);
                    floatActionButton.setLayoutParams(params);
                } else if (dy > 0) {
                    floatActionButton.hide();
                    params.setMargins(0, 0, 24, 32);
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    params.addRule(RelativeLayout.ALIGN_PARENT_END);
                    floatActionButton.setLayoutParams(params);
                }

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    floatActionButton.show();
                }
//                else if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
//                    floatActionButton.hide();
//                }
            }
        });
        floatActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                layoutManager.scrollToPositionWithOffset(0, 2);
                Toast.makeText(getContext(), "click movie page", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void sendID() {
        SharedPreferences sp = getActivity().getSharedPreferences("MOVIE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("ID", 1);
//        editor.clear(); // delete
//        editor.remove("ID") // delete id
        editor.apply();
    }

    private void acceptID() {
        SharedPreferences sp = getActivity().getSharedPreferences("MOVIE", Context.MODE_PRIVATE);
        sp.getInt("ID", 1);
    }

    @Override
    public void setClickItem(int id, String title) {
        SharedPreferences sp = getActivity().getSharedPreferences("MOVIE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("ID", id);
        editor.putString("TITLE", title);
        editor.apply();
        startActivity(new Intent(getContext(), MovieDetailActivity.class));
//        Snackbar.make(coordinatorLayout, "ID MOVIE : "+id, Snackbar.LENGTH_SHORT).show();
    }
}
