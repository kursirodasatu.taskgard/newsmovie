package com.kursirodasatu.newsapi.movie;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kursirodasatu.newsapi.GlideApp;
import com.kursirodasatu.newsapi.R;
import com.kursirodasatu.newsapi.pojo.movie.Movie;

public class MoviePageAdapter extends PagedListAdapter<Movie.Results, MovieHolder> {

    private Context mCtx;
    private ClickItem clickItem;
    MoviePageAdapter(Context mCtx, ClickItem clickItem){
        super(DIFF_CALLBACK);
        this.mCtx = mCtx;
        this.clickItem = clickItem;
    }

    interface ClickItem{
        void setClickItem(int id, String title);
    }
    @NonNull
    @Override
    public MovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.item_movie, parent, false);
        return new MovieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieHolder holder, int position) {
        final Movie.Results item = getItem(position);
        if (item != null){
            try {
                GlideApp.with(mCtx)
                        .load("http://image.tmdb.org/t/p/w500"+item.poster_path)
                        .error(R.drawable.image_null)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(false)
                        .apply(RequestOptions.centerCropTransform())
                        .into(holder.ivMoviePoster);
                holder.tvMovieRating.setText(String.valueOf(item.vote_average));
                holder.tvMovieName.setText(item.title);
                holder.rlMovie.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clickItem.setClickItem(item.id, item.title);
                    }
                });
            }catch (Exception e){

            }

        }
    }

    private static DiffUtil.ItemCallback<Movie.Results> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<Movie.Results>() {
                @Override
                public boolean areItemsTheSame(Movie.Results oldItem, Movie.Results newItem) {
                    return oldItem.id==newItem.id;
                }

                @Override
                public boolean areContentsTheSame(Movie.Results oldItem, Movie.Results newItem) {
                    return oldItem.original_title.equals(newItem.original_title);
                }
            };
}
