package com.kursirodasatu.newsapi.movie;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kursirodasatu.newsapi.GlideApp;
import com.kursirodasatu.newsapi.R;
import com.kursirodasatu.newsapi.pojo.movie.DetailMovie;
import com.kursirodasatu.newsapi.pojo.movie.Genre;

import java.util.List;

public class ProductionAdapter extends RecyclerView.Adapter<ProductionHolder> {

    private Context context;
    private List<DetailMovie.Production_companies> production_companies;

    public ProductionAdapter(Context context) {
        this.context = context;
    }

    public void setProduction(List<DetailMovie.Production_companies> production_companies) {
        this.production_companies = production_companies;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_movie_production, parent, false);
        return new ProductionHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductionHolder holder, int position) {
        DetailMovie.Production_companies item = production_companies.get(position);
        try{
            GlideApp.with(context)
                    .load("http://image.tmdb.org/t/p/w300"+item.logo_path)
                    .error(R.drawable.image_null)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(false)
                    .apply(RequestOptions.fitCenterTransform())
                    .into(holder.ivProductionLogo);
            holder.tvProductionName.setText(item.name);
        }catch (Exception e){

        }

    }

    @Override
    public int getItemCount() {
        if (production_companies != null){
            return production_companies.size();
        }else {
            return 0;
        }
    }
}
