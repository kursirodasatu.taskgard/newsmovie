package com.kursirodasatu.newsapi.movie;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.kursirodasatu.newsapi.R;

public class GenreHolder extends RecyclerView.ViewHolder {
    TextView tvGenreName, tvGenreID;
    public GenreHolder(View itemView) {
        super(itemView);
        tvGenreName = (TextView) itemView.findViewById(R.id.tvGenreName);
        tvGenreID = (TextView) itemView.findViewById(R.id.tvGenreId);
    }
}
