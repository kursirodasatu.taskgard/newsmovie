package com.kursirodasatu.newsapi.movie;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kursirodasatu.newsapi.R;

public class ProductionHolder extends RecyclerView.ViewHolder{
    TextView tvProductionName;
    ImageView ivProductionLogo;
    public ProductionHolder(View view) {
        super(view);
        tvProductionName = (TextView) view.findViewById(R.id.tvProductionName);
        ivProductionLogo = (ImageView) view.findViewById(R.id.ivProductionLogo);
    }
}
