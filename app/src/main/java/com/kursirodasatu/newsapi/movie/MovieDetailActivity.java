package com.kursirodasatu.newsapi.movie;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kursirodasatu.newsapi.GlideApp;
import com.kursirodasatu.newsapi.R;
import com.kursirodasatu.newsapi.pojo.movie.DetailMovie;
import com.kursirodasatu.newsapi.retrofit.viewmodel.DetailMovieApiViewModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieDetailActivity extends AppCompatActivity implements Observer<DetailMovie> {

    @BindView(R.id.ivBackDrop)
    AppCompatImageView ivBackDrop;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.collapsingToolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.ivPoster)
    ImageView ivPoster;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvOverview)
    TextView tvOverview;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;


    @BindView(R.id.recyclerViewDetail)
    RecyclerView recyclerViewDetail;


    ProductionAdapter adapter;
    DetailAdapter detailAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white);
        final SharedPreferences sp = getSharedPreferences("MOVIE", MODE_PRIVATE);
        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset <= 100) {
                    collapsingToolbar.setTitle(sp.getString("TITLE", "musketeerz"));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });

        DetailMovieApiViewModel model = ViewModelProviders.of(this).get(DetailMovieApiViewModel.class);
        model.getData(sp.getInt("ID", 346910)).observe(this, this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setHasFixedSize(true);
        adapter = new ProductionAdapter(getApplicationContext());
        recyclerView.setAdapter(adapter);

        recyclerViewDetail.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerViewDetail.setHasFixedSize(true);
        detailAdapter = new DetailAdapter(getApplicationContext());
        recyclerViewDetail.setAdapter(detailAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        SharedPreferences sp = getSharedPreferences("MOVIE", MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.apply();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onChanged(@Nullable DetailMovie detailMovie) {
        GlideApp.with(getApplicationContext())
                .load("http://image.tmdb.org/t/p/w500" + detailMovie.poster_path)
                .error(R.drawable.image_null)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(false)
                .apply(RequestOptions.fitCenterTransform())
                .into(ivPoster);

        GlideApp.with(getApplicationContext())
                .load("http://image.tmdb.org/t/p/w780" + detailMovie.backdrop_path)
                .error(R.drawable.image_null)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(false)
                .apply(RequestOptions.centerCropTransform())
                .into(ivBackDrop);
        String tanggal = detailMovie.release_date;
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format = new SimpleDateFormat("d MMMM yyyy");
//        SimpleDateFormat format = new SimpleDateFormat("EEEE, dd MMMM yyyy");
        Calendar c = Calendar.getInstance();

        try {
            c.setTime(format1.parse(tanggal));
            tanggal = format.format(c.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }


        String genre = "";
        for (int i = 0; i < detailMovie.genres.size(); i++) {
            genre += detailMovie.genres.get(i).name + ", ";
        }
        int lenGenre = genre.length();
        genre = genre.substring(0, lenGenre - 2);

        tvTitle.setText(detailMovie.title);
        tvOverview.setText(detailMovie.overview);

        adapter.setProduction(detailMovie.production_companies);

        Map<String, String> mapRelease = new HashMap<>();
        mapRelease.put("Release Date : ", tanggal);

        Map<String, String> mapDuration = new HashMap<>();
        mapDuration.put("Duration : ", detailMovie.runtime + " Minute");

        Map<String, String> mapGenre = new HashMap<>();
        mapGenre.put("Genre : ", genre);

        List<Map<String, String>> details = new ArrayList<>();
        details.add(mapRelease);
        details.add(mapDuration);
        details.add(mapGenre);

        detailAdapter.setDetail(details);
    }
}
