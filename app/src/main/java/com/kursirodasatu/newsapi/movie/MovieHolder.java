package com.kursirodasatu.newsapi.movie;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kursirodasatu.newsapi.R;

public class MovieHolder extends RecyclerView.ViewHolder{
    TextView tvMovieName, tvMovieRating;
    ImageView ivMoviePoster;
    RelativeLayout rlMovie;
    public MovieHolder(View view) {
        super(view);
        tvMovieName = (TextView) view.findViewById(R.id.tvMovieName);
        tvMovieRating = (TextView) view.findViewById(R.id.tvMovieRating);
        ivMoviePoster = (ImageView) view.findViewById(R.id.ivMoviePoster);
        rlMovie = (RelativeLayout) view.findViewById(R.id.rlMovie);
    }
}
