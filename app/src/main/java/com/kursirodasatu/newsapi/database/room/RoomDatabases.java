package com.kursirodasatu.newsapi.database.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.kursirodasatu.newsapi.database.dao.GenreDao;
import com.kursirodasatu.newsapi.pojo.movie.Genre;

@Database(entities = {Genre.Genres.class}, version = 1)
public abstract class RoomDatabases extends RoomDatabase {
    private static RoomDatabases mInstance;

    public abstract GenreDao genreDao();

    public static RoomDatabases getDatabase(Context context){
        if(mInstance == null){
            synchronized (RoomDatabases.class){
                if(mInstance == null){
                    mInstance = Room.databaseBuilder(context.getApplicationContext(), RoomDatabases.class, "kursirodasatu")
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return mInstance;
    }
}
