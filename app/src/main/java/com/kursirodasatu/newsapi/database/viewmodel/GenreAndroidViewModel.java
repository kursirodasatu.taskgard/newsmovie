package com.kursirodasatu.newsapi.database.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.kursirodasatu.newsapi.database.repository.GenreRepository;
import com.kursirodasatu.newsapi.pojo.movie.Genre;

import java.util.List;

public class GenreAndroidViewModel extends AndroidViewModel {
    private GenreRepository mGenreRepository;
    private LiveData<List<Genre.Genres>> mLiveData;

    public GenreAndroidViewModel(@NonNull Application application) {
        super(application);
        mGenreRepository = new GenreRepository(application);
        mLiveData = mGenreRepository.getAllGenres();
    }

    public LiveData<List<Genre.Genres>> getAllGenres(){
        return mLiveData;
    }

    public void insertAll(List<Genre.Genres> genres){
        mGenreRepository.insertAll(genres);
    }

    public void insertOne(Genre.Genres genres){
        mGenreRepository.insertOne(genres);
    }
}
