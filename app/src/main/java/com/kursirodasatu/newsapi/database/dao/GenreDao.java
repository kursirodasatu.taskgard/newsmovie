package com.kursirodasatu.newsapi.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.kursirodasatu.newsapi.pojo.movie.Genre;

import java.util.List;

@Dao
public interface GenreDao {

    String TABLE_NAME = "genre_table";
    String KEY_ID = "id";
    String KEY_NAME = "name";

    @Insert
    void insertOne(Genre.Genres genres);

    @Insert
    void insertAll(List<Genre.Genres> genres);

    @Query("DELETE FROM "+TABLE_NAME)
    void deleteAll();

    @Query("SELECT * FROM "+TABLE_NAME+" ORDER BY "+KEY_NAME)
    LiveData<List<Genre.Genres>> getAllGenre();
}
