package com.kursirodasatu.newsapi;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.kursirodasatu.newsapi.article.ArticleFragment;
import com.kursirodasatu.newsapi.article.ArticlePageFragment;
import com.kursirodasatu.newsapi.setting.SettingPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.appBar)
    AppBarLayout appBar;
    @BindView(R.id.viewPager)
    ViewPager viewPager;


    private SettingPagerAdapter settingPagerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white);
        actionBar.setTitle("NewaApi.org");

        settingPagerAdapter = new SettingPagerAdapter(getSupportFragmentManager());
        setupViewPager(viewPager, new DetailFragment(), "Detail", 0);
        setupViewPager(viewPager, new DetailFragment(), "Nadir", 1);
        setupViewPager(viewPager, new ArticleFragment(), "Top Headline", 2);
        setupViewPager(viewPager, new ArticlePageFragment(), "Pemilu", 3);
        setupViewPager(viewPager, new ArticlePageFragment(), "Phone", 4);
        setupViewPager(viewPager, new ArticlePageFragment(), "Motocycle", 5);

        tabs.setupWithViewPager(viewPager);

        int limit = (settingPagerAdapter.getCount() > 1 ? settingPagerAdapter.getCount() - 1 : 1);
        viewPager.setOffscreenPageLimit(limit);

    }


    private void setupViewPager(ViewPager viewPager, Fragment fragment, String title, int position) {
        Bundle bundle = new Bundle();
        if(position == 3 || position == 4 || position == 5) {
            bundle.putString("question", title);
        }
        fragment.setArguments(bundle);
        settingPagerAdapter.addFragment(fragment, title);
        viewPager.setAdapter(settingPagerAdapter);
        viewPager.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
