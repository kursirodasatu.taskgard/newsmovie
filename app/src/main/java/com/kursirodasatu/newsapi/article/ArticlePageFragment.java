package com.kursirodasatu.newsapi.article;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.kursirodasatu.newsapi.R;
import com.kursirodasatu.newsapi.pojo.article.Article;
import com.kursirodasatu.newsapi.retrofit.viewmodel.ArticlePageApiViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ArticlePageFragment extends Fragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    Unbinder unbinder;

    @BindView(R.id.floatActionButton)
    FloatingActionButton floatActionButton;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_main, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        final ArticlePageAdapter adapter = new ArticlePageAdapter(getContext());

        String question = "nadir";
        try {
            question = getArguments().getString("question");

        }catch (Exception e){

        }

        ArticlePageApiViewModel model = ViewModelProviders.of(this).get(ArticlePageApiViewModel.class);
        model.getPagedList(question).observe(this, new Observer<PagedList<Article.Articles>>() {
            @Override
            public void onChanged(@Nullable PagedList<Article.Articles> articlesItems) {
                adapter.submitList(articlesItems);
            }
        });

        recyclerView.setAdapter(adapter);


        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(0,0,16, 32);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        params.addRule(RelativeLayout.ALIGN_PARENT_END);
        floatActionButton.setLayoutParams(params);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT
                );
                int aku = floatActionButton.getMeasuredWidth();
                if(dy < 0){
                    floatActionButton.hide();
                    params.setMargins(0,0,24, 32+aku);
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    params.addRule(RelativeLayout.ALIGN_PARENT_END);
                    floatActionButton.setLayoutParams(params);
                }else if(dy > 0){
                    floatActionButton.hide();
                    params.setMargins(0,0,24, 32);
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    params.addRule(RelativeLayout.ALIGN_PARENT_END);
                    floatActionButton.setLayoutParams(params);
                }

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    floatActionButton.show();
                }
//                else if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
//                    floatActionButton.hide();
//                }
            }
        });
        floatActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                layoutManager.scrollToPositionWithOffset(0, 2);
                Toast.makeText(getContext(), "click page", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
