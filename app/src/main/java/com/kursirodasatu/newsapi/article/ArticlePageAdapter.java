package com.kursirodasatu.newsapi.article;

import android.arch.paging.PagedListAdapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.util.DiffUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kursirodasatu.newsapi.GlideApp;
import com.kursirodasatu.newsapi.R;
import com.kursirodasatu.newsapi.pojo.article.Article;

public class ArticlePageAdapter extends PagedListAdapter<Article.Articles, ArticleHolder> {

    private Context mCtx;

    ArticlePageAdapter(Context mCtx) {
        super(DIFF_CALLBACK);
        this.mCtx = mCtx;
    }

    @NonNull
    @Override
    public ArticleHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.item_article, parent, false);
        return new ArticleHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleHolder holder, int position) {
        Article.Articles item = getItem(position);
        if(item != null){
            try {
                GlideApp.with(mCtx)
                        .load(item.urlToImage)
                        .error(R.drawable.image_null)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(false)
                        .apply(RequestOptions.centerCropTransform())
                        .into(holder.ivArticleFoto);
                holder.tvArticleAuthor.setText(item.author);
                holder.tvArticleTitle.setText(item.title);
                holder.tvArticleDescription.setText(item.description);
                holder.tvArticleNo.setText(String.valueOf(position+1));
            }catch (Exception e){

            }

        }else {
            Snackbar.make(holder.itemView, "Item Is null...", Snackbar.LENGTH_SHORT).show();
        }
    }

    private static DiffUtil.ItemCallback<Article.Articles> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<Article.Articles>() {
                @Override
                public boolean areItemsTheSame(Article.Articles oldItem, Article.Articles newItem) {
//                    by id item article
                    return oldItem.description.equals(newItem.description);
                }

                @Override
                public boolean areContentsTheSame(Article.Articles oldItem, Article.Articles newItem) {
                    return oldItem.equals(newItem);
                }
            };
}
