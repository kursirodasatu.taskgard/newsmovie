package com.kursirodasatu.newsapi.article;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kursirodasatu.newsapi.R;

public class ArticleHolder extends RecyclerView.ViewHolder {
    ImageView ivArticleFoto;
    TextView tvArticleAuthor, tvArticleDescription, tvArticleTitle, tvArticleNo;
    public ArticleHolder(View itemView) {
        super(itemView);
        ivArticleFoto = (ImageView) itemView.findViewById(R.id.ivArticleFoto);
        tvArticleAuthor = (TextView) itemView.findViewById(R.id.tvArticleAuthor);
        tvArticleDescription = (TextView) itemView.findViewById(R.id.tvArticleDescription);
        tvArticleTitle = (TextView) itemView.findViewById(R.id.tvArticleTitle);
        tvArticleNo = (TextView) itemView.findViewById(R.id.tvArticleNo);
    }
}
