package com.kursirodasatu.newsapi.article;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.kursirodasatu.newsapi.GlideApp;
import com.kursirodasatu.newsapi.R;
import com.kursirodasatu.newsapi.pojo.article.Article;

import java.util.List;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleHolder>{

    private Context mCtx;
    private List<Article.Articles> items;

    public ArticleAdapter(Context mCtx) {
        this.mCtx = mCtx;
    }


    @NonNull
    @Override
    public ArticleHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.item_article, parent, false);
        return new ArticleHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleHolder holder, int position) {
        Article.Articles item = items.get(position);
        try{
            GlideApp.with(mCtx)
                    .load(item.urlToImage)
                    .error(R.drawable.image_null)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(false)
                    .apply(RequestOptions.centerCropTransform())
                    .into(holder.ivArticleFoto);
            holder.tvArticleAuthor.setText(item.author);
            holder.tvArticleTitle.setText(item.title);
            holder.tvArticleDescription.setText(item.description);
            holder.tvArticleNo.setText(String.valueOf(position+1));
        }catch (Exception e){

        }

    }

    public void setItems(List<Article.Articles> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(items != null){
            return items.size();
        }else {
            return 0;
        }

    }


}
