package com.kursirodasatu.newsapi.retrofit.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.kursirodasatu.newsapi.pojo.article.Article;
import com.kursirodasatu.newsapi.retrofit.RetrofitClient;
import com.kursirodasatu.newsapi.retrofit.api.ApiArticle;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleApiViewModel extends ViewModel {

    private MutableLiveData<Article> articles;
    private String category;
    private String country;
    public LiveData<Article> getData(String country, String category){
        this.country = country;
        this.category = category;
        if(articles == null){
            articles = new MutableLiveData<>();
            loadTopHeadLines();
        }
        return articles;
    }
    private void loadTopHeadLines(){
        RetrofitClient.getInstanceArticle()
                .getApiArticle()
                .getTopHeadLines(country, category, ApiArticle.API_KEY)
                .enqueue(new Callback<Article>() {
                    @Override
                    public void onResponse(Call<Article> call, Response<Article> response) {
                        articles.setValue(response.body());
                    }

                    @Override
                    public void onFailure(Call<Article> call, Throwable t) {

                    }
                });
    }


}
