package com.kursirodasatu.newsapi.retrofit;

import com.kursirodasatu.newsapi.retrofit.api.ApiArticle;
import com.kursirodasatu.newsapi.retrofit.api.ApiMovie;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private Retrofit retrofit;
    private static RetrofitClient mInstanceArticle;
    private static RetrofitClient mInstanceMovie;

    private RetrofitClient(String base) {
        retrofit = new Retrofit.Builder()
                .baseUrl(base)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized RetrofitClient getInstanceArticle(){
        if(mInstanceArticle == null){
            mInstanceArticle = new RetrofitClient(ApiArticle.BASE_URL);
        }
        return mInstanceArticle;
    }

    public static synchronized RetrofitClient getInstanceMovie(){
        if(mInstanceMovie == null){
            mInstanceMovie = new RetrofitClient(ApiMovie.BASE_URL);
        }
        return mInstanceMovie;
    }

    public ApiArticle getApiArticle(){
        return retrofit.create(ApiArticle.class);
    }

    public ApiMovie getApiMovie(){
        return retrofit.create(ApiMovie.class);
    }

//    public static Retrofit getClient(String url){
//        if(retrofit == null){
//            retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
//        }
//        return retrofit;
//    }
}
