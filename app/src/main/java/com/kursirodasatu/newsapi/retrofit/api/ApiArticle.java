package com.kursirodasatu.newsapi.retrofit.api;


import com.kursirodasatu.newsapi.pojo.article.Article;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiArticle {
    String BASE_URL = "https://newsapi.org/v2/";
    String API_KEY = "b0061cd7f46443a58553563eb1dffa9c";

    @GET("top-headlines")
    Call<Article> getTopHeadLines(
            @Query("country") String country,
            @Query("category") String category,
            @Query("apiKey") String apiKey
            );

    @GET("everything")
    Call<Article> getEverythingPage(
            @Query("page") int page,
            @Query("q") String q,
            @Query("sortBy") String sortBy,
            @Query("apiKey") String apiKey
    );
}
