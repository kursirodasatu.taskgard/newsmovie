package com.kursirodasatu.newsapi.retrofit.datasource;

import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;

import com.kursirodasatu.newsapi.pojo.movie.Movie;
import com.kursirodasatu.newsapi.retrofit.RetrofitClient;
import com.kursirodasatu.newsapi.retrofit.api.ApiMovie;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoviePageApiDataSource extends PageKeyedDataSource<Integer, Movie.Results> {
    public static final int FIRST_PAGE = 1;
    public static final int PAGE_SIZE = 20;
    final String SHORT_BY = "popularity.desc";
    final String LANGUAGE = "en-US";
    final boolean INCLUDE_ADULT = false;
    final boolean INCLUDE_VIDEO = false;

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull final LoadInitialCallback<Integer, Movie.Results> callback) {
        RetrofitClient.getInstanceMovie()
                .getApiMovie()
                .getPageMovie(ApiMovie.API_KEY, LANGUAGE, SHORT_BY, INCLUDE_ADULT, INCLUDE_VIDEO, FIRST_PAGE)
                .enqueue(new Callback<Movie>() {
                    @Override
                    public void onResponse(Call<Movie> call, Response<Movie> response) {
                        if(response.body() != null){
                            callback.onResult(response.body().results, null, FIRST_PAGE +1);
                        }
                    }

                    @Override
                    public void onFailure(Call<Movie> call, Throwable t) {

                    }
                });
    }

    @Override
    public void loadBefore(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Movie.Results> callback) {
        RetrofitClient.getInstanceMovie()
                .getApiMovie()
                .getPageMovie(ApiMovie.API_KEY, LANGUAGE, SHORT_BY, INCLUDE_ADULT, INCLUDE_VIDEO, params.key)
                .enqueue(new Callback<Movie>() {
                    @Override
                    public void onResponse(Call<Movie> call, Response<Movie> response) {
                        Integer adjacentKey = (params.key > 1) ? params.key - 1 : null;
                        if(response.body() != null){
                            callback.onResult(response.body().results, adjacentKey);
                        }
                    }

                    @Override
                    public void onFailure(Call<Movie> call, Throwable t) {

                    }
                });
    }

    @Override
    public void loadAfter(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Movie.Results> callback) {
        RetrofitClient.getInstanceMovie()
                .getApiMovie()
                .getPageMovie(ApiMovie.API_KEY, LANGUAGE, SHORT_BY, INCLUDE_ADULT, INCLUDE_VIDEO, params.key)
                .enqueue(new Callback<Movie>() {
                    @Override
                    public void onResponse(Call<Movie> call, Response<Movie> response) {
                        if(response.body() != null){
                            callback.onResult(response.body().results, params.key + 1);
                        }
                    }

                    @Override
                    public void onFailure(Call<Movie> call, Throwable t) {

                    }
                });
    }
}
