package com.kursirodasatu.newsapi.retrofit.datasource;

import android.arch.paging.PageKeyedDataSource;
import android.support.annotation.NonNull;

import com.kursirodasatu.newsapi.pojo.article.Article;
import com.kursirodasatu.newsapi.retrofit.RetrofitClient;
import com.kursirodasatu.newsapi.retrofit.api.ApiArticle;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticlePageApiDataSource extends PageKeyedDataSource<Integer, Article.Articles> {

    public static final int FIRST_PAGE = 1;
    public static final int PAGE_SIZE = 20;
    private String QUESTION = "phone";
    public static final String SHORT_BY = "publishedAt";

    public ArticlePageApiDataSource(String question) {
        this.QUESTION = question;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull final LoadInitialCallback<Integer, Article.Articles> callback) {
        RetrofitClient.getInstanceArticle()
                .getApiArticle()
                .getEverythingPage(FIRST_PAGE, QUESTION, SHORT_BY, ApiArticle.API_KEY)
                .enqueue(new Callback<Article>() {
                    @Override
                    public void onResponse(Call<Article> call, Response<Article> response) {
                        if(response.body() != null){
                            callback.onResult(response.body().articles, null, FIRST_PAGE +1);
                        }
                    }

                    @Override
                    public void onFailure(Call<Article> call, Throwable t) {

                    }
                });
    }

    @Override
    public void loadBefore(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Article.Articles> callback) {
        RetrofitClient.getInstanceArticle()
                .getApiArticle()
                .getEverythingPage(params.key, QUESTION, SHORT_BY, ApiArticle.API_KEY)
                .enqueue(new Callback<Article>() {
                    @Override
                    public void onResponse(Call<Article> call, Response<Article> response) {
                        Integer adjacentKey = (params.key > 1) ? params.key - 1 : null;
                        if(response.body() != null){
                            callback.onResult(response.body().articles, adjacentKey);
                        }
                    }

                    @Override
                    public void onFailure(Call<Article> call, Throwable t) {

                    }
                });
    }

    @Override
    public void loadAfter(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Article.Articles> callback) {
        RetrofitClient.getInstanceArticle()
                .getApiArticle()
                .getEverythingPage(params.key, QUESTION, SHORT_BY, ApiArticle.API_KEY)
                .enqueue(new Callback<Article>() {
                    @Override
                    public void onResponse(Call<Article> call, Response<Article> response) {
                        if(response.body() != null){
                            callback.onResult(response.body().articles, params.key + 1);
                        }
                    }

                    @Override
                    public void onFailure(Call<Article> call, Throwable t) {

                    }
                });
    }
}
