package com.kursirodasatu.newsapi.pojo.article;

import java.util.List;


public class Article {

    public List<Articles> articles;
    public int totalResults;
    public String status;

    public class Articles {
        public String publishedAt;
        public String urlToImage;
        public String url;
        public String description;
        public String title;
        public String author;
        public Source source;
    }

    public class Source {
        public String name;
        public String id;
    }
}
