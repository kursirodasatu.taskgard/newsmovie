package com.kursirodasatu.newsapi.pojo.movie;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.kursirodasatu.newsapi.database.dao.GenreDao;

import java.util.List;


public class Genre {

    public List<Genres> genres;

    @Entity(tableName = GenreDao.TABLE_NAME)
    public static class Genres {

        @ColumnInfo(name = GenreDao.KEY_NAME)
        public String name;

        @PrimaryKey
        @ColumnInfo(name = GenreDao.KEY_ID)
        public int id;
    }
}
