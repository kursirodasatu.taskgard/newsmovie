package com.kursirodasatu.newsapi.pojo.article;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class COBAS {

    @SerializedName("results")
    public List<Results> results;
    @SerializedName("total_pages")
    public int total_pages;
    @SerializedName("total_results")
    public int total_results;
    @SerializedName("page")
    public int page;

    public static class Results {
        @SerializedName("release_date")
        public String release_date;
        @SerializedName("overview")
        public String overview;
        @SerializedName("adult")
        public boolean adult;
        @SerializedName("backdrop_path")
        public String backdrop_path;
        @SerializedName("genre_ids")
        public List<Integer> genre_ids;
        @SerializedName("original_title")
        public String original_title;
        @SerializedName("original_language")
        public String original_language;
        @SerializedName("poster_path")
        public String poster_path;
        @SerializedName("popularity")
        public double popularity;
        @SerializedName("title")
        public String title;
        @SerializedName("vote_average")
        public double vote_average;
        @SerializedName("video")
        public boolean video;
        @SerializedName("id")
        public int id;
        @SerializedName("vote_count")
        public int vote_count;
    }
}
