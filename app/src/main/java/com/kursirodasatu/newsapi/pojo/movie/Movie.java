package com.kursirodasatu.newsapi.pojo.movie;

import java.util.List;

public class Movie {

    public List<Results> results;
    public int total_pages;
    public int total_results;
    public int page;

    public static class Results {
        public String release_date;
        public String overview;
        public boolean adult;
        public String backdrop_path;
        public List<Integer> genre_ids;
        public String original_title;
        public String original_language;
        public String poster_path;
        public double popularity;
        public String title;
        public double vote_average;
        public boolean video;
        public int id;
        public int vote_count;
    }
}
