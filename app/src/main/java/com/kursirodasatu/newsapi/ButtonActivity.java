package com.kursirodasatu.newsapi;

import android.app.Activity;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.kursirodasatu.newsapi.database.viewmodel.GenreAndroidViewModel;
import com.kursirodasatu.newsapi.pojo.movie.Genre;
import com.kursirodasatu.newsapi.retrofit.viewmodel.GenreApiViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ButtonActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.tvMovie)
    TextView tvMovie;
    @BindView(R.id.tvArticle)
    TextView tvArticle;
    @BindView(R.id.tvMain)
    TextView tvMain;

    public static final int HOME_ACTIVITY_RC = 1;
    public static final int MAIN_ACTIVITY_RC = 2;
    public static final int GENRE_ACTIVITY_RC = 3;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_button);
        ButterKnife.bind(this);

        tvArticle.setOnClickListener(this);
        tvMain.setOnClickListener(this);
        tvMovie.setOnClickListener(this);

        final GenreAndroidViewModel genreAndroidViewModel = ViewModelProviders.of(this).get(GenreAndroidViewModel.class);
        genreAndroidViewModel.getAllGenres().observe(this, new Observer<List<Genre.Genres>>() {
            @Override
            public void onChanged(@Nullable List<Genre.Genres> genres) {
                assert genres != null;
                if(genres.size() == 0) {
                    addGenre(genreAndroidViewModel);
                }
            }
        });

    }

    private void addGenre(final GenreAndroidViewModel genreAndroidViewModel){
        final GenreApiViewModel model = ViewModelProviders.of(this).get(GenreApiViewModel.class);
        model.getData().observe(this, new Observer<Genre>() {
            @Override
            public void onChanged(@Nullable Genre genre) {
                assert genre != null;
                genreAndroidViewModel.insertAll(genre.genres);
            }
        });
    }


    @Override
    public void onClick(View v) {
        if(v == tvArticle){
            Intent intent = new Intent(this, HomeActivity.class);
            startActivityForResult(intent, HOME_ACTIVITY_RC);
        }else if(v == tvMain){
            Intent intent = new Intent(this, MainActivity.class);
            startActivityForResult(intent, MAIN_ACTIVITY_RC);
        }else if(v == tvMovie){
            Intent intent = new Intent(this, GenreActivity.class);
            startActivityForResult(intent, GENRE_ACTIVITY_RC);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == HOME_ACTIVITY_RC){

        }else if(requestCode == MAIN_ACTIVITY_RC){

        }else if(requestCode == GENRE_ACTIVITY_RC){

        }
    }
}
